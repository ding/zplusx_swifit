#path="../../data1/Zmm.root"
#python AutoFit.py --path $path

#path="/eos/user/d/ding/SWAN_projects/ZplusX/data3/ZmmX.root"
#path="/eos/user/d/ding/SWAN_projects/ZplusX/data3/ZmmZX.root"
#path="/eos/user/d/ding/SWAN_projects/ZplusX/data3/ZmmHT.root"
#path="/afs/cern.ch/work/d/ding/ZplusX/data4/mass.root"
#path="/eos/user/d/ding/SWAN_projects/ZplusX/data5/DATAmass.root"
#path="/eos/user/d/ding/SWAN_projects/ZplusX/data8/files/ZEE.root"
#path="/eos/user/d/ding/SWAN_projects/ZplusX/data9/Zee_data1516_global.root"
#path="/global/homes/w/wding/zplusx_swifit/eos/data9/Scale/Zee_mc16ad_SR_Scale0p8.root"
#path="/global/homes/w/wding/zplusx_swifit/eos/data9/Reduced/Zee_mc16ad_CR.root"
#path="/global/homes/w/wding/zplusx_swifit/eos/data12/NewSpectrumZjetsSherpa.root"
#path="/global/homes/w/wding/zplusx_swifit/eos/data11/fluctuated/BkgPlusSignalScale100_Seed2000.root"
#path="/global/homes/w/wding/zplusx_swifit/eos/spurious/MCBkgs_FatJ.root"
path="/global/homes/w/wding/zplusx_swifit/eos/data13/SR.root"
#path="/global/homes/w/wding/zplusx_swifit/eos/data13/spurious/data13LeadBSR_bkg.root"
#save="data9ZeeMC16adSRScale0p8"
#save="data10NewPlots"
#save="SpuFatJGaus10per"
#save="data11SplusB100"
#save="data3ZmmX"
#save="data3ZmmZX"
#save="data12ZjetsSherpa"
save="data13LeadFatJSR_Gauss15per_V2"
#save="data13LeadJSR_Gauss3per"
txt=$save
python AutoFit.py --path $path --save $save --txt $txt  #SWIFIT and Spurious
#python AutoFitZX.py --path $path --save $save --txt $txt
#python AutoFitSingle1.py --path $path --save $save --txt $txt #Save function
#python AutoFitSingle2.py --path $path --save $save --txt $txt #up and down

