#!/bin/bash


cd /global/homes/w/wding
source atlas.sh

cd /global/homes/w/wding/zplusx_swifit/code/ 


for i in $(seq 1 500)
do
echo $i
path1="../eos/data14/LeadFatJSR/LeadFatJSR_bkg_seed"$i".root"
path2="../eos/data14/LeadJSR/LeadJSR_bkg_seed"$i".root"
path3="../eos/data14/LeadBSR/LeadBSR_bkg_seed"$i".root"
save="Gauss7Func1per_SR_seed"$i
txt=$save

python AutoFitLeadFatJ.py --path $path1 --save $save --txt $txt  #SWIFIT and Spurious
python AutoFitLeadJ.py --path $path2 --save $save --txt $txt  #SWIFIT and Spurious
python AutoFitLeadB.py --path $path3 --save $save --txt $txt  #SWIFIT and Spurious

done



