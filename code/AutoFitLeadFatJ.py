from ROOT import *
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
parser.add_argument("--save", dest='save',  default="", help="save")
parser.add_argument("--txt", dest='txt',  default="", help="txt")
args = parser.parse_args()

histlist=[]
filepath=args.path
filename=filepath.split("/")[-1].split(".")[0]
f=TFile(filepath,"r")

histname="mc_sherpaZ_SR_highZPt100_LeadFatJ"
for key in f.GetListOfKeys():
    if  histname in key.GetName():  
        histlist.append(key.GetName())

confdir="/global/homes/w/wding/zplusx_swifit/configFiles"
rundir="/global/homes/w/wding/zplusx_swifit/code"
conf="config_fitLeadFatJ.txt"
#conf="config_Xmass.txt"

savedirname=args.save
savedir1="/global/homes/w/wding/zplusx_swifit/output/pdf"
savedir2="/global/homes/w/wding/zplusx_swifit/output/root"
if not os.path.exists(savedir2+"/"+savedirname):
  os.mkdir(savedir1+"/"+savedirname)
  os.mkdir(savedir2+"/"+savedirname)

for hist in histlist:
  #outname=filename+"_"+hist
  outname=hist
  print outname
  keyline1="inputFile       : "+filepath+"\n"
  keyline2="inputHist       : "+hist+"\n"
  keyline3="pdfName         : pdf/"+savedirname+"/"+outname+"\n"
  keyline4="oRootName       : root/"+savedirname+"/"+outname+"\n"

  os.chdir(confdir)
  confnew=open("configTemp1.txt","w")
  with open(conf, "r") as fin:
    for line in fin:
      if line.startswith("inputFile"):
        line=keyline1
        confnew.write(line)
      elif line.startswith("inputHist "):
        line=keyline2
        confnew.write(line)
      elif line.startswith("pdfName"):
        line=keyline3
        confnew.write(line)
      elif line.startswith("oRootName"):
        line=keyline4
        confnew.write(line)
      else:
        confnew.write(line)
  fin.close()
  confnew.close()

  os.chdir(rundir)
  #output=os.popen("./runSWiFt.sh ../configFiles/configTemp1.txt") #SWifit
  output = os.popen("./runfit.sh ../configFiles/configTemp1.txt") #Spurious
  
  txtfile=args.txt+".txt"
  Record=open("./TxtPseudo/"+txtfile,"a")
  outxt=list(output.readlines())
  #print outxt[-2]  
  Line1=filepath+": "+hist+"\n"
  Line2=outxt[-1]
  Line3=outxt[-2]
  Line4=outxt[-3]
  Record.write(Line1)
  Record.write(Line2)
  Record.write(Line3)
  Record.write(Line4)  
  Record.write("\n")  
  Record.close()

  os.remove(confdir+"/configTemp1.txt")












