from ROOT import *
from array import array
import argparse,math,os
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--seed", dest='seed',  default="", help="seed")
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

path=args.path
seed=args.seed
filename=path.split("/")[-1]
outpath=filename.split(".")[0]+"_Seed"+seed+".root"
histlist=[]
f=TFile.Open(path,"r")
for i in f.GetListOfKeys():
    histlist.append(i.GetName())
print histlist,outpath
for j in histlist:
    hist=f.Get(j)
    grandom=TRandom3(int(seed))
    for k in range(hist.GetNbinsX()):
        bin_cont = hist.GetBinContent(k)
        random=grandom.Poisson(bin_cont)
        hist.SetBinContent(k,random)
        hist.SetBinError( k, math.sqrt(random) )
    outFile = TFile.Open(outpath,'UPDATE')
    outFile.cd()
    hist.Write(hist.GetName(),TObject.kOverwrite)
    outFile.Close()

'''
def flu():
    #f=TFile.Open("/eos/user/d/ding/RUN2/SWiFtFit/root/data1516/qstar_data1516_LLHScan4p_24.root")
    #f=TFile.Open("/eos/user/d/ding/RUN2/MC16d_DL1rFixed77.root")
    #f=TFile.Open("/afs/cern.ch/user/d/ding/SWiFt/inputs/DibABCDmjj.root")
    f=TFile.Open(args.path)
    #hist=f.Get("swiftBkgBest")
    #hist.Scale(140.0/37.0)
    #hist=f.Get("Onetag_ABCD_DL1r_mjj")
    hist=f.Get(args.hist)
    hist1=hist.Clone()
    hist1.SetNameTitle(args.title+"_"+args.seed,args.title+"_"+args.seed)


    grandom=TRandom3(int(args.seed))
    for i in range(0,hist.GetNbinsX()):
     #if hist.GetBinLowEdge(i)>=1200 and hist.GetBinLowEdge(i)<=6165:
      #TRandom3 s(20000)
      #hist1=hist.Clone()
      bin_cont = hist.GetBinContent(i)
      random=grandom.Poisson(bin_cont)
      #if hist1.GetBinLowEdge(i)<=6047:
      #if hist1.GetBinLowEdge(i)<7889:
      hist1.SetBinContent(i,random)
      hist1.SetBinError( i, math.sqrt(random) )
     #else :
      #hist1.SetBinContent(i,0)
      #hist1.SetBinError( i, 0 )
    #for i in range(1,hist1.GetNbinsX()+1):
      #if hist1.GetBinLowEdge(i)<1200:
        #hist1.SetBinContent(i,random)
    #outFile = TFile.Open('mbbseed2000test.root','UPDATE')
    outFile = TFile.Open(args.outname,'UPDATE')
    outFile.cd()
    hist1.Write(hist1.GetName(),TObject.kOverwrite)
    outFile.Close()





if __name__=="__main__":
    flu()


'''


