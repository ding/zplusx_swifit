#!/bin/bash
hadd output_Zee_sherpa_Zjets_mc16a_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16a/output/output_Zee_Zjets_mc16a_*_miniTree_LLCR.root
hadd output_Zee_top_mc16a_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16a/output/output_Zee_top_mc16a_*_miniTree_LLCR.root
hadd output_Zee_Vg_mc16a_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16a/output/output_Zee_Vg_mc16a_*_miniTree_LLCR.root
hadd output_Zee_multiboson_mc16a_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16a/output/output_Zee_VV*_mc16a_*_miniTree_LLCR.root
hadd output_Zee_Ztautau_mc16a_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16a/output/output_Zee_Ztautau_mc16a_*_miniTree_LLCR.root
hadd output_Zee_Wjets_mc16a_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16a/output/output_Zee_Wjets_mc16a_*_miniTree_LLCR.root
