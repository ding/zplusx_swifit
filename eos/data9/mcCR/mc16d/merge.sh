#!/bin/bash
hadd output_Zee_sherpa_Zjets_mc16d_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16d/output/output_Zee_Zjets_mc16d_*_miniTree_LLCR.root
hadd output_Zee_top_mc16d_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16d/output/output_Zee_top_mc16d_*_miniTree_LLCR.root
hadd output_Zee_Vg_mc16d_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16d/output/output_Zee_Vg_mc16d_*_miniTree_LLCR.root
hadd output_Zee_multiboson_mc16d_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16d/output/output_Zee_VV*_mc16d_*_miniTree_LLCR.root
hadd output_Zee_Ztautau_mc16d_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16d/output/output_Zee_Ztautau_mc16d_*_miniTree_LLCR.root
hadd output_Zee_Wjets_mc16d_CR.root /net/ustc_01/users/kuhan/ZX/Condor/nominal-miniTree/ee-CR/mc16d/output/output_Zee_Wjets_mc16d_*_miniTree_LLCR.root
