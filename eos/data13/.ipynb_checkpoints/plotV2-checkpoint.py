from ROOT import *
from array import array
import argparse,math,os,glob
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

#plot
path="/global/homes/w/wding/zplusx_swifit/eos/data13/spurious/"
paths=glob.glob(path+"*root")
Path=paths[5]
#Path=""
print paths,Path
f=TFile.Open(Path,"r")
hists=[]
category="Zmm_mc_sherpaZ_SR_highZPt100_LeadFatJ_ZXmass"
for i in f.GetListOfKeys():
    #if category in i.GetName():
        hists.append(i.GetName())
print hists
j=0
for i in hists:
    print i,j
    j=j+1
hist=hists[int(args.path)]
print hist

h1=f.Get(hist)
h1.GetYaxis().SetRangeUser(-0.2,0.4)
h1.SetNameTitle("","")
h1.SetTitle("")
h1.SetMarkerStyle(8)
h1.SetMarkerColor(1)
h1.SetLineColor(1)
#print(h1.GetMarkerSize())
#h1.SetMarkerSize(1.6)
if "_ZX" in hist:
    h1.GetXaxis().SetTitle("m_{ZX} [GeV]")
if "_X" in hist:
    h1.GetXaxis().SetTitle("m_{X} [GeV]")
h1.GetYaxis().SetTitle("S/#deltaS")
#print(h1.GetXaxis().GetTitleSize())
h1.GetXaxis().SetTitleSize(0.04)
h1.GetYaxis().SetTitleSize(0.04)
#print h1
c1=TCanvas("c1","c1",100,0,900,700)
#c1.SetLogy()
gStyle.SetOptStat(0)

h1.SetNameTitle("","")

c1.cd()
h1.Draw()


l=TLatex()
l.SetTextSize(0.04)
l.SetTextAlign(13)
#print(l.GetTextSize())
#LegendLow=2800
#LegendLow1=1000
LegendLow=2400
LegendLow1=500
LegendBottom=0.2
#l.DrawLatex(LegendLow,LegendBottom*0.15,"#it{ATLAS} #bf{Work in Progress}")
l.DrawLatex(LegendLow1,LegendBottom*1.6,"#it{ATLAS} #bf{Internal}")
l.DrawLatex(LegendLow1,LegendBottom*1.4,"#bf{#sqrt{s} = 13TeV, 140fb^{-1}}")
l.DrawLatex(LegendLow,LegendBottom*1.6,"#bf{MC, SR}")
if "ee" in hist:
    #l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrowe^{+}e^{-}, LeadFatJ}")
    #l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrowe^{+}e^{-}, LeadJ}")
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrowe^{+}e^{-}, LeadB}")
if "mm" in hist:
    #l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrow#mu^{+}#mu^{-}, LeadFatJ}")
    #l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrow#mu^{+}#mu^{-}, LeadJ}")
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrow#mu^{+}#mu^{-}, LeadB}")

if "Gauss3per" in hist and "_X" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{x}/m_{x}=3%}")
elif "Gauss5per" in hist and "_X" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{x}/m_{x}=5%}")
elif "Gauss7per" in hist and "_X" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{x}/m_{x}=7%}")
elif "Gauss10per" in hist and "_X" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{x}/m_{x}=10%}")
elif "Gauss15per" in hist and "_X" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{x}/m_{x}=15%}")
    
elif "Gauss3per" in hist and "_ZX" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{zx}/m_{zx}=3%}")
elif "Gauss5per" in hist and "_ZX" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{zx}/m_{zx}=5%}")
elif "Gauss7per" in hist and "_ZX" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{zx}/m_{zx}=7%}")
elif "Gauss10per" in hist and "_ZX" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{zx}/m_{zx}=10%}")
elif "Gauss15per" in hist and "_ZX" in hist:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{zx}/m_{zx}=15%}")
    
l.SetTextSize(0.035)
    
c1.Draw()
c1.SaveAs("figures1/"+Path.split("/")[-1].split(".")[0]+"_"+hist+".jpg")
c1.SaveAs("figures1/"+Path.split("/")[-1].split(".")[0]+"_"+hist+".pdf")


