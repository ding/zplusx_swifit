#!/usr/bin/env python
import os,math
from array import array
from ROOT import *
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--t", dest='threeparam',  default="", help="threeparam")
parser.add_argument("--fourparam", dest='fourparam',  default="", help="fourparam")
args = parser.parse_args()
def FitUn4p():
    #fbkg=TFile("/afs/cern.ch/user/d/ding/SWiFt/output/root/mc/data_20163para_ForUncert.root")
    #fbkg=TFile("/afs/cern.ch/user/d/ding/SWiFt/output/pdf/fulldata/WStarfulldata20width_end7018.root")
    fbkg=TFile("/afs/cern.ch/user/d/ding/SWiFt/output/root/fulldata/WStarfulldataNew.root")
    #hbkg=fbkg.Get("swiftBkgBest")
    hbkg=fbkg.Get("swiftBkgNominal")   
    #hbkg=fbkg.Get("globalFitNominal")
    N=hbkg.GetNbinsX()
    #for i in range(80):
      #print i,hbkg.GetBinContent(i),hbkg.GetBinLowEdge(i)
        
    print args.threeparam
    PEdir="/afs/cern.ch/work/d/ding/RUN2/SWiFtFit/condor/"+args.threeparam+"/root"
    #PEdir="/eos/user/y/yuxu/wei/condor2/"+args.threeparam+"/root"
    PElist=[]
    Unlist=[]
    print PEdir
    for f in os.listdir(PEdir):     
      if os.path.getsize(PEdir+"/"+f)>=10000:
        PElist.append(PEdir+"/"+f)
    print len(PElist)
    
    mean=[]
    Uncern=[]
    for i in range(75):
      Uncern.append(0.0)
      mean.append(0.0)

    for f in PElist:
      fPE=TFile(f)
      hPE=fPE.Get("swiftBkgNominal")
      for i in range(1,76):
        mean[i-1]=mean[i-1]+hPE.GetBinContent(i)
      fPE.Close()
     
    for i in range(75):
      mean[i]=mean[i]/(len(PElist)*1.00)
      #print (mean[i]-hbkg.GetBinContent(i+1))/hbkg.GetBinContent(i+1)
     

    
    for f in PElist:
      fPE=TFile(f)
      hPE=fPE.Get("swiftBkgNominal")
      #print hPE.GetBinLowEdge(42)
      for i in range(1,76):
        #a=pow((hbkg.GetBinContent(i)-hPE.GetBinContent(i)),2)
        #Uncern[i-1]=Uncern[i-1]+abs(hPE.GetBinContent(i)-mean[i-1])
        Uncern[i-1]=Uncern[i-1]+pow((hPE.GetBinContent(i)-mean[i-1]),2)
      fPE.Close()
   
    for i in range(75):
      #Uncern[i]=Uncern[i]/(len(PElist)*hbkg.GetBinContent(i+1))
      Uncern[i]=math.sqrt(Uncern[i]/len(PElist))/hbkg.GetBinContent(i+1)
    #for i in range(93):
      #print Uncern[i],hbkg.GetBinLowEdge(i+42)
    print Uncern
    fbkg.Close()
     
    bins=[  1717, 1767, 1818, 1870, 1923, 1977, 2032, 2088, 2145, 2203, 2262, 2322, 2384, 2447, 2511, 2576, 2642, 2709, 2778, 2848, 2919, 2991, 3065, 3140, 3217, 3295, 3374, 3455, 3537, 3621, 3706, 3793, 3882, 3972, 4064, 4158, 4253, 4350, 4449, 4550, 4653, 4758, 4865, 4974, 5085, 5198, 5313, 5430, 5549, 5670, 5794, 5920, 6048, 6178, 6311, 6447, 6585, 6727, 6871, 7018, 7168, 7322, 7478, 7638, 7801, 7967, 8136, 8309, 8486, 8666, 8850, 9037, 9228, 9423, 9622]
    #bins=[1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208]
    #bins=[1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 423, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756,7904,8055,8208]
    #bins = [ 203, 216, 229, 243, 257, 272, 287, 303, 319, 335, 352, 369, 387, 405, 424, 443, 462, 482, 502, 523, 544, 566, 588, 611, 634, 657, 681, 705, 730, 755, 781, 807, 834, 861, 889, 917, 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 423, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 1341 ]
    
    h1 = TH1F( "wjj4paraFitUncerntainty","wjj4paraFitUncerntainty",len(bins)-1,array('f', bins) )
    #print len(bins)
    for i in range(1,75):
    #  print i,h1.GetBinLowEdge(i)
      h1.SetBinContent(i,Uncern[i-1])
    outFile = TFile.Open('FulldataNew.root','UPDATE')
    outFile.cd()
    h1.Write(h1.GetName(),TObject.kOverwrite)
    outFile.Close()
    





   







if __name__=="__main__":
    FitUn4p()







