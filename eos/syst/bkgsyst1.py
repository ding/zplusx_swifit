import ROOT,os,os,glob,math,os
from array import array
import numpy as np
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

path="/global/project/projectdirs/atlas/massDecorrelatedXbb/ZplusX/root/"
rootlist=os.listdir(path+"Gauss15per_SR_seed1/")
rootpath=rootlist[int(args.path)]
paths=glob.glob(path+"Gauss15per_SR_seed*/"+rootpath)
outpath="syst.root"
bkgpath="/global/homes/w/wding/zplusx_swifit/eos/syst/bkg.root"
fbkg=ROOT.TFile(bkgpath,"r")
hbkg=fbkg.Get(rootpath.split(".")[0])
print(len(paths),rootpath)

mean=[0 for i in range(100)]
Uncern=[0 for i in range(100)]

for f in paths: 
    fPE=ROOT.TFile(f,"r")
    #for i in fPE.GetListOfKeys():
        #print(i.GetName())
    hPE=fPE.Get("globalFitNominal")
    for i in range(0,100):
            mean[i]=mean[i]+hPE.GetBinContent(i)
            #print(i,hPE.GetBinContent(i),hPE.GetBinLowEdge(i),hbkg.GetBinLowEdge(i))
    fPE.Close()

mean=[i/(len(paths)*1.0) for i in mean]

for f in paths:
    fPE=ROOT.TFile(f,"r")
    hPE=fPE.Get("globalFitNominal")
    for i in range(0,100):
            Uncern[i]=Uncern[i]+pow((hPE.GetBinContent(i)-mean[i]),2)
    fPE.Close()

Uncern=[math.sqrt(Uncern[i]/len(paths))/hbkg.GetBinContent(i) if hbkg.GetBinContent(i)!=0 else 0 for i in range(100)]

h1=hbkg.Clone()
name1=hbkg.GetName()+"_syst1"
h1.SetNameTitle(name1,name1)
for i in range(0,100):
    h1.SetBinContent(i,Uncern[i])
outFile = ROOT.TFile.Open(outpath,'UPDATE')
outFile.cd()
h1.Write(h1.GetName(),ROOT.TObject.kOverwrite)
outFile.Close()


