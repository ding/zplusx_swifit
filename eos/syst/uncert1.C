#include "TError.h"
#include "helperFunctions/fitFunctions.C"
#include "helperFunctions/histPlotFormatFunctions.C"
#include "helperFunctions/statFitFunctions.C"
#include "helperFunctions/windowSelector1.C"
#include "helperFunctions/swiftBkgGenerator.C"
#include "../ATLASStyle/AtlasStyle.C"                                                                             
#include "../ATLASStyle/AtlasUtils.C"

using namespace std;
using namespace TMath;


void uncert1()
{
  SetAtlasStyle ();


//Get hists
  //TString datafile="/eos/user/d/ding/RUN2/dibData_mjj_new.root";
  //TString bkgfile="/afs/cern.ch/user/d/ding/SWiFt/output/root/fulldata/dijet_fulldata_errortype1_0.root";
  //TString uncertfile="/afs/cern.ch/user/d/ding/SWiFt/plotting/resonanceSearch/Uncerntainty.root";
  //TString uncertfile1="/afs/cern.ch/user/d/ding/SWiFt/plotting/resonanceSearch/UncerntaintyNew1.root";
  //TString datafile="/eos/user/d/ding/RUN2/Wstar_140ifb.root"; 
  //TString bkgfile="/afs/cern.ch/user/d/ding/SWiFt/output/pdf/fulldata/WStarfulldata20width_end7018.root";
  //TString uncertfile="/afs/cern.ch/user/d/ding/SWiFt/plotting/resonanceSearch/UncerntaintyWStarFulldata.root";
  TString datafile="/afs/cern.ch/work/d/ding/public/Dijet/Binbin/FulldataNew.root";
  TString dataname="data_mjj";
  TString bkgname="bkg_mjj";
  TString funcert="mjjFunctionChoiceUncerntainty"; 
  TString fitcert="mjj4paraFitUncerntainty";
   
  TFile *f1    = TFile::Open( datafile, "r" );
  f1 -> cd();
  TH1D *data = (TH1D*)f1->Get(dataname);
  TH1D *h1 = (TH1D*)data -> Clone();
  //f1->Close();
  
  //TFile *f2    = TFile::Open( bkgfile, "r" );
  //f2 -> cd();
  TH1D *bkg = (TH1D*)f1->Get(bkgname);
  TH1D *h2 = (TH1D*)bkg -> Clone();
  //f2->Close();

  //TFile *f3    = TFile::Open( uncertfile, "r" );
  //TFile *f4    = TFile::Open( uncertfile1, "r" );
  //f3 -> cd();
  //f4->cd();
  //TH1D *u1 = (TH1D*)f3->Get(funcert);
  TH1D *u1 = (TH1D*)f1->Get(funcert);
  TH1D *u2 = (TH1D*)f1->Get(fitcert);
   
  TH1D *h3 = (TH1D*)u1 -> Clone();
  TH1D *h4 = (TH1D*)u2 -> Clone();
  TH1D *h5 = (TH1D*)u1-> Clone();
  TH1D *h6 = (TH1D*)u2-> Clone();

  //f3->Close();


  cout<<"This is 1"<<endl;
  
//plots
  TCanvas *canvas = new TCanvas();
  canvas -> Update();
  gStyle -> SetOptFit();
  //canvas -> Print("PDF[");
  gPad   -> SetLeftMargin(0.12);
  gPad   -> SetBottomMargin(0.12);

  //==// set up pads 
  TPad *pad1 = new TPad("pad1","pad1 ", 0, 0.30, 1, 1);
  pad1 -> SetBottomMargin(0);
  pad1 -> SetLeftMargin(0.12);
  pad1 -> SetLogy(1);
  
  //pad1 -> SetLogx(1); //
  pad1 -> Draw();
  
  TPad *pad2 = new TPad("pad2","pad2", 0, 0.01, 1, 0.30);
  pad2->SetTopMargin(0.00001);
  pad2->SetBottomMargin(0.4);
  pad2 -> SetLeftMargin(0.12);
  pad2 -> SetLogy(0);
  //pad2 -> SetLogx(1); //
  pad2->Draw();

  cout<<"This is 2"<<endl;
  
  pad1    -> cd();
  formatHist(h1, "Events");
  //formatHist(h2);
  cout<<"This is 2.55"<<endl;
  h2->SetLineColor(kRed);
  h2->GetYaxis()->SetTitleOffset(0.8);
  h1->GetYaxis()->SetTitleOffset(0.8);
  
  h1->GetXaxis()->SetRangeUser(1100, 8055);
  h2->GetXaxis()->SetRangeUser(1100, 8055);

  h2 -> Draw("][ hist");
  h1 -> Draw("e1 same");
  cout<<"This is 2.5"<<endl;
  drawText( 0.6, 0.85, "#bf{#it{ATLAS}} Internal", kBlack, 0.050);
  
  drawText( 0.20, 0.30, "#sqrt{s} = 13TeV, 140fb^{-1}", kBlack, 0.040);
  
  //drawText( 0.75, 0.80, legLabel1, hist->GetLineColor(), 0.040);
  //drawText( 0.75, 0.75, legLabel2, bkgHist->GetLineColor(), 0.040);
  drawText( 0.20, 0.25, "#chi^{2} p-value = 0.07", kBlack, 0.040);
  //drawText( 0.20, 0.25, "#chi^{2} p-value = 0.13", kBlack, 0.040);
  drawText( 0.20, 0.20, "Fit Range: 1.1-8.1 TeV", kBlack, 0.040);
  //drawText( 0.20, 0.20, "Fit Range: 1.7-9.6 TeV", kBlack, 0.040);
  drawText( 0.20, 0.15, "|y*|<0.6", kBlack, 0.040);
  //drawText( 0.20, 0.15, "|y*|<1.2", kBlack, 0.040);  
  TLegend* leg = new TLegend(0.6,0.6,0.9,0.8);
  leg->AddEntry(h1,"Data","lp");
  leg->AddEntry(h2,"Fit","l");
  leg->AddEntry(h3,"Function choice","l");
  leg->AddEntry(h4,"Statistical uncertainty on fit","l");
  leg->SetTextFont(42);
  leg->SetTextSize(0.04);
  leg->SetBorderSize(0);
  leg->SetLineColor(0);
  leg->SetLineStyle(1);
  leg->SetLineWidth(1);
  leg->SetFillColor(0);
  leg->SetFillStyle(0);

  leg->Draw();
   

  //TGaxis* axis =new  TGaxis(1100, 0.3,8208 , 0.3, 20, 220, 510, "");
  //TGaxis* axis =new  TGaxis(1717, 0.3,9622 , 0.3, 20, 220, 510, "");
  //axis->SetLabelFont(43);
  //axis->SetLabelSize(15);
  //axis->Draw();


  pad2 -> cd();
  //TH1D *histDiff = (TH1D*)hist -> Clone("histDiff");
  //histDiff -> SetLineWidth(2);
  //histDiff -> Add(bkgHist, -1);
  //divideUncert(histDiff, hist );
  formatDiff(h3, "Rel. Uncert", "m_{jj} [GeV]", 1,kBlue);
  formatDiff(h4, "", "", 1,kBlue+1);
  setBestRangeTH1(h3, 0.3);
  h3 -> GetYaxis() -> SetTitleSize(12);
  h3 -> GetYaxis() -> SetLabelSize(12);
  h3 -> GetXaxis() -> SetMoreLogLabels();
  h3->SetLineStyle(4);
  h4->SetLineStyle(7);
  h3->SetLineColor(38);
  h4->SetLineColor(9);
  h3->Sumw2();
  h3->SetStats(0);
  //h4->Sumw2();
  h4->SetStats(0);
  
  //TH1D *h5 = (TH1D*)h3-> Clone(); 
  //TH1D *h6 = (TH1D*)h4-> Clone();
  h5->Scale(-1.0);
  formatDiff(h5, "", "", 1,38);
  h5->SetLineStyle(4);
  h5->SetStats(0);
  //h5->Sumw2();
  h6->Scale(-1.0);
  formatDiff(h6, "", "", 1,9);
  h6->SetLineStyle(7);
  h6->SetStats(0);

  h4->GetXaxis()->SetRangeUser(1100, 8055);
  h3->GetXaxis()->SetRangeUser(1100, 8055);
  h6->GetXaxis()->SetRangeUser(1100, 8055);


  h3 -> Draw("][ hist");
  h4->Draw("SAME");
  //h5->Draw("hist SAME");
  h6->Draw("hist SAME");
  
  makeTLine(1100, 0, 8055, 0, kBlack, "hist same", 1);
  //makeTLine(1717, 0, 9622, 0, kBlack, "hist same", 1);
  cout<<"This is 3"<<endl;

  canvas->SaveAs("uncertNew.pdf");


}













