import ROOT,os,os,glob,math,os
from array import array
import numpy as np
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

path="/global/project/projectdirs/atlas/massDecorrelatedXbb/ZplusX/root/"
rootlist=os.listdir(path+"Gauss15per_SR_seed1/")
rootpath=rootlist[int(args.path)]
paths1=glob.glob(path+"Gauss3per_SR_seed*/"+rootpath)
paths2=glob.glob(path+"Gauss15per_SR_seed*/"+rootpath)
outpath="syst.root"
bkgpath="/global/homes/w/wding/zplusx_swifit/eos/syst/bkg.root"
fbkg=ROOT.TFile(bkgpath,"r")
hbkg=fbkg.Get(rootpath.split(".")[0])
print(len(paths1),rootpath)

mean=[0 for i in range(100)]
Uncern=[0 for i in range(100)]
for i in range(len(paths1)):
    fPE1=ROOT.TFile(paths1[i],"r")
    hPE1=fPE1.Get("globalFitNominal")
    PE2path=paths1[i].replace("3per","15per")
    fPE2=ROOT.TFile(PE2path,"r")
    hPE2=fPE2.Get("globalFitNominal")
    for j in range(100):
        Uncern[j]=Uncern[j]+hPE1.GetBinContent(j)-hPE2.GetBinContent(j)
    fPE1.Close()
    fPE2.Close()
Uncern=[abs(Uncern[i]/(len(paths1)*hbkg.GetBinContent(i))) if hbkg.GetBinContent(i)!=0 else 0 for i in range(100)]  

h1=hbkg.Clone()
name1=hbkg.GetName()+"_syst2"
h1.SetNameTitle(name1,name1)
for i in range(0,100):
    h1.SetBinContent(i,Uncern[i])
outFile = ROOT.TFile.Open(outpath,'UPDATE')
outFile.cd()
h1.Write(h1.GetName(),ROOT.TObject.kOverwrite)
outFile.Close()



