import ROOT,os,glob,math
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()
path=args.path
scalelist=[1,5,10,20,50,100]
scale=scalelist[int(path)]
newfile="BkgPlusSignalScale"+str(scale)+".root"
command="hadd "+newfile+" MCBkgs.root "
for i in range(scale):
    command=command+"FatJSignal1400.root "
#print newfile
print command
os.system(command)
