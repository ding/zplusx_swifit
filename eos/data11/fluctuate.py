from ROOT import *
from array import array
import argparse,math,os
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--seed", dest='seed',  default="", help="seed")
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

path=args.path
seed=args.seed
filename=path.split("/")[-1]
outpath="fluctuated/"+filename.split(".")[0]+"_Seed"+seed+".root"
histlist=[]
f=TFile.Open(path,"r")
for i in f.GetListOfKeys():
    histlist.append(i.GetName())
print histlist,outpath
for j in histlist:
    hist=f.Get(j)
    grandom=TRandom3(int(seed))
    for k in range(hist.GetNbinsX()):
        bin_cont = hist.GetBinContent(k)
        random=grandom.Poisson(bin_cont)
        hist.SetBinContent(k,random)
        hist.SetBinError( k, math.sqrt(random) )
    outFile = TFile.Open(outpath,'UPDATE')
    outFile.cd()
    hist.Write(hist.GetName(),TObject.kOverwrite)
    outFile.Close()



