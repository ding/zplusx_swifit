import ROOT,os,glob,math
from array import array
import numpy as np
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

path="./pull/"
paths=glob.glob(path+"*root")
count=int(args.path)
rootpath=paths[count]
f=ROOT.TFile(rootpath,"r")

r1=ROOT.TFitResult()
r2=ROOT.TFitResult()
r3=ROOT.TFitResult()
r4=ROOT.TFitResult()
r5=ROOT.TFitResult()
r6=ROOT.TFitResult()
r7=ROOT.TFitResult()
h1=f.Get("pull_1GeV")
h2=f.Get("pull_1p5GeV")
h3=f.Get("pull_2GeV")
h4=f.Get("pull_2p5GeV")
h5=f.Get("pull_3GeV")
h6=f.Get("pull_3p5GeV")
h7=f.Get("pull_4GeV")
rebin=8
h1.Rebin(rebin)
h2.Rebin(rebin)
h3.Rebin(rebin)
h4.Rebin(rebin)
h5.Rebin(rebin)
h6.Rebin(rebin)
h7.Rebin(rebin)
r1=h1.Fit("gaus","s")
r2=h2.Fit("gaus","s")
r3=h3.Fit("gaus","s")
r4=h4.Fit("gaus","s")
r5=h5.Fit("gaus","s")
r6=h6.Fit("gaus","s")
r7=h7.Fit("gaus","s")
mean=[]
std=[]
mass=[1,1.5,2,2.5,3,3.5,4]
mean.append(r1.Parameter(1))
std.append(r1.Parameter(2))
mean.append(r2.Parameter(1))
std.append(r2.Parameter(2))
mean.append(r3.Parameter(1))
std.append(r3.Parameter(2))
mean.append(r4.Parameter(1))
std.append(r4.Parameter(2))
mean.append(r5.Parameter(1))
std.append(r5.Parameter(2))
mean.append(r6.Parameter(1))
std.append(r6.Parameter(2))
mean.append(r7.Parameter(1))
std.append(r7.Parameter(2))

x=array("f",mass)
y=array("f",mean)
ex=array("f",[0,0,0,0,0,0,0])
ey=array("f",std)
g=ROOT.TGraphErrors(len(mass),x,y,ex,ey)
g.SetTitle("")
g.SetMarkerColor(4)
g.SetLineColor(4)
g.SetMarkerStyle(20)
g.GetYaxis().SetRangeUser(-3,3)
if "_ZX" in rootpath:
    g.GetXaxis().SetTitle("m_{ZX} [GeV]")
if "_X" in rootpath:
    g.GetXaxis().SetTitle("m_{X} [GeV]")
g.GetYaxis().SetTitle("<pull>")
c1=ROOT.TCanvas("c1","c1",100,0,900,700)
c1.SetGrid()
c1.GetFrame().SetFillColor(21)
c1.GetFrame().SetBorderSize(12)
ROOT.gStyle.SetOptStat(0)
c1.cd()
g.Draw("ALP")


#Legend
l=ROOT.TLatex()
l.SetTextSize(0.04)
l.SetTextAlign(13)
LegendLow=2.5
LegendLow1=1
LegendBottom=1.7
l.DrawLatex(LegendLow1,LegendBottom*1.6,"#it{ATLAS} #bf{Internal}")
l.DrawLatex(LegendLow1,LegendBottom*1.4,"#bf{#sqrt{s} = 13TeV, 140fb^{-1}}")
l.DrawLatex(LegendLow1,LegendBottom*1.2,"#bf{Spurious signal test}")
l.DrawLatex(LegendLow,LegendBottom*1.6,"#bf{MC, SR}")
if "ee" in rootpath and "LeadB" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrowe^{+}e^{-}, LeadB}")
if "mm" in rootpath and "LeadB" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrow#mu^{+}#mu^{-}, LeadB}")
if "ee" in rootpath and "LeadJ" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrowe^{+}e^{-}, LeadJ}")
if "mm" in rootpath and "LeadJ" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrow#mu^{+}#mu^{-}, LeadJ}")
if "ee" in rootpath and "LeadFatJ" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrowe^{+}e^{-}, LeadFatJ}")
if "mm" in rootpath and "LeadFatJ" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.4,"#bf{Z#rightarrow#mu^{+}#mu^{-}, LeadFatJ}")
if  "_X" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{x}/m_{x}=3%}")
if "_ZX" in rootpath:
    l.DrawLatex(LegendLow,LegendBottom*1.2,"#bf{Gaussian signal, #sigma_{zx}/m_{zx}=3%}")

l.SetTextSize(0.02)

c1.Draw()
figurename="figures/"+rootpath.split("/")[-1].split(".")[0]
c1.SaveAs(figurename+".jpg")
c1.SaveAs(figurename+".pdf")



