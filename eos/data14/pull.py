import ROOT,os,glob,math
from array import array
import numpy as np
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

count=int(args.path)
path="/global/u1/w/wding/zplusx_swifit/output/root/"
rootlist=os.listdir(path+"Gauss3per_SR_seed1/")
rootpath=rootlist[count]
paths=glob.glob(path+"Gauss3per_SR_seed*/"+rootpath)
outpath="/global/homes/w/wding/zplusx_swifit/eos/data14/pull/"+rootpath
print(len(paths),rootpath)
h0=ROOT.TH1F("pull_1GeV","pull_1GeV",100,-5,5)
h1=ROOT.TH1F("pull_1p5GeV","pull_1p5GeV",100,-5,5)
h2=ROOT.TH1F("pull_2GeV","pull_2GeV",100,-5,5)
h3=ROOT.TH1F("pull_2p5GeV","pull_2p5GeV",100,-5,5)
h4=ROOT.TH1F("pull_3GeV","pull_3GeV",100,-5,5)
h5=ROOT.TH1F("pull_3p5GeV","pull_3p5GeV",100,-5,5)
h6=ROOT.TH1F("pull_4GeV","pull_4GeV",100,-5,5)
#1000GeV 2500GeV 4000GeV
binlist=[]
binlistX=[970 ,1450 ,1973 ,2462 ,3041 ,3478 ,3954]  #[970,2462,3954]
binlistZX=[1011 ,1500 ,1955 ,2535 ,3056 ,3445 ,4091] #[1011,2535,4091]
for i in paths:
    if "_Xmass." in i:
        binlist=binlistX
    else:
        binlist=binlistZX
    f=ROOT.TFile.Open(i,"r")
    Signal=f.Get("ExtractedSignal")
    SignalError=f.Get("ExtractedSignalError")
    pull0=np.nan_to_num(Signal.Eval(binlist[0])/(SignalError.Eval(binlist[0]*1.0)))
    pull1=np.nan_to_num(Signal.Eval(binlist[1])/(SignalError.Eval(binlist[1]*1.0)))
    pull2=np.nan_to_num(Signal.Eval(binlist[2])/(SignalError.Eval(binlist[2]*1.0)))
    pull3=np.nan_to_num(Signal.Eval(binlist[3])/(SignalError.Eval(binlist[3]*1.0)))
    pull4=np.nan_to_num(Signal.Eval(binlist[4])/(SignalError.Eval(binlist[4]*1.0)))
    pull5=np.nan_to_num(Signal.Eval(binlist[5])/(SignalError.Eval(binlist[5]*1.0)))
    pull6=np.nan_to_num(Signal.Eval(binlist[6])/(SignalError.Eval(binlist[6]*1.0))) 
    h0.Fill(pull0)
    h1.Fill(pull1)
    h2.Fill(pull2)
    h3.Fill(pull3)
    h4.Fill(pull4)
    h5.Fill(pull5)
    h6.Fill(pull6)    
outFile = ROOT.TFile.Open(outpath,'UPDATE')
outFile.cd()
h0.Write(h0.GetName(),ROOT.TObject.kOverwrite)
h1.Write(h1.GetName(),ROOT.TObject.kOverwrite)
h2.Write(h2.GetName(),ROOT.TObject.kOverwrite)
h3.Write(h3.GetName(),ROOT.TObject.kOverwrite)
h4.Write(h4.GetName(),ROOT.TObject.kOverwrite)
h5.Write(h5.GetName(),ROOT.TObject.kOverwrite)
h6.Write(h6.GetName(),ROOT.TObject.kOverwrite)
outFile.Close()
f.Close()




